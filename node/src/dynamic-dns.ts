import { readFileSync } from 'fs'
import axios from 'axios'
import {
	setIntervalAsync,
	clearIntervalAsync,
	SetIntervalAsyncTimer,
} from 'set-interval-async/dynamic'

export type IpAddress = `${number}.${number}.${number}.${number}`

const GOOGLE_DOMAIN_API = 'https://domains.google.com/nic/update'

export class DynamicDns {
	private _runnerHandle?: SetIntervalAsyncTimer

	/** Authentication for Google Domain API */
	private _auth: {
		username: string
		password: string
	}
	/** Desired hostname */
	private _hostname: string
	/** Interval in which to check IP (ms) */
	private _updateInterval: number

	/** @param updateInterval Interval in which to periodically check if the IP needs updating (ms) */
	constructor(configPath: string = './conf.json') {
		// Load config
		const { username, password, hostname, interval } = JSON.parse(
			readFileSync(configPath, 'utf-8'),
		)
		// Apply config settings
		this._hostname = hostname
		this._auth = { username, password }
		this._updateInterval = interval * 60 * 1000
	}

	/** Sets the IP address for Google Domains */
	async setIpAddress(ip?: IpAddress) {
		return axios.get(GOOGLE_DOMAIN_API, {
			params: {
				hostname: this._hostname,
				myip: ip,
			},
			auth: this._auth,
		})
	}

	async updateDns() {
		const setResp = await this.setIpAddress()
		console.log(setResp.data)
	}

	/** Starts the Dynamic DNS updater */
	async start() {
		console.log('Running Dynamic DNS updater')
		await this.updateDns()
		this._runnerHandle = setIntervalAsync(
			this.updateDns.bind(this),
			this._updateInterval,
		)
	}

	/** Stops the Dynamic DNS updater */
	async stop() {
		if (!this._runnerHandle) return
		await clearIntervalAsync(this._runnerHandle)
		this._runnerHandle = undefined
		console.log('Stopped Dynamic DNS updater')
	}
}

if (require.main === module) {
	const dynDns = new DynamicDns()
}
