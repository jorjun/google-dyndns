import { DynamicDns } from './dynamic-dns'
export { DynamicDns } from './dynamic-dns'

if (require.main === module) {
	const dynamicDns = new DynamicDns()
	dynamicDns.start()
}
