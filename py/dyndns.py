"""
@jorjun Vvi Sol in Virgo, Luna in Pisces (full)
"""
import time
from datetime import datetime
import json
import requests
import logging

# import http.client as http_client
# http_client.HTTPConnection.debuglevel = 1
# logging.basicConfig()
# logging.getLogger().setLevel(logging.DEBUG)
# requests_log = logging.getLogger("requests.packages.urllib3")
# requests_log.setLevel(logging.DEBUG)
# requests_log.propagate = True


class FailedInit(Exception):
    pass


class FailedUpdate(Exception):
    pass


class Post:
    def __init__(self, conf_path="conf.json"):
        try:
            with open(conf_path) as _json:
                js = json.loads(_json.read())
                for fld in ("username", "password", "hostname", "interval"):
                    setattr(self, fld, js[fld])

                self.interval_minutes = int(self.interval) * 60
        except FileNotFoundError as exc:
            print(f"Failed to find configuration: `{conf_path}`")
            raise FailedInit(
                "Rename `sampleconf.json` to `conf.json` with real credentials in there?"
            )
        except Exception as exc:
            raise FailedInit(f"Failed to load `{conf_path}` with error: {exc}")

        rep = (
            "Dynamic DNS service:",
            f"Host: {self.hostname}",
            f"Interval: {self.interval} mins",
        )
        print("\n\t".join(rep))

    def get_ip_address(self):
        response = requests.get("https://ipv4bot.whatismyipaddress.com")
        return response.content.decode()

    def set_ip_address(self):
        headers = {
            "User-Agent": "@jorjun",
        }
        resp = requests.post(
            f"https://domains.google.com/nic/update?hostname={self.hostname}",
            headers=headers,
            auth=(self.username, self.password),
        )
        return resp.content.decode()

    @staticmethod
    def fmt_log(msg):
        ts = datetime.utcnow().strftime("%H:%M:%S")
        print(f"GMT {ts} - {msg}")

    def loop(self):
        while True:
            status = self.set_ip_address()
            self.fmt_log(f"DYNDNS status: {status}")
            if not any([_ in status for _ in ("good", "nochg")]):
                raise FailedUpdate("Failed IP update")
            time.sleep(self.interval_minutes)


if __name__ == "__main__":
    try:
        post = Post()
        post.loop()
    except Exception as exc:
        print(f"\n{exc}")
        print("Aborting...")
