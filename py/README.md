Dynamic Domain Name Service
============================

"Updates your google-hosted dynamic name service (DNS)"

Uses: ipv4bot.whatismyipaddress.com to obtain your router's current IP address (darn thing keeps changing so you can't access your home Pi when you are elsewhere, right?)

And: domains.google.com/nic/update to perform the update.


* INSTALL *

1. Change to this software folder:
>cd ~/src/google-dyndns (or where you cloned/downloaded this folder)

2. Packaged with `python poetry`, so follow: https://python-poetry.org/docs/#installation

3. Create virtual environment to install package dependencies without disrupting your other installs (or to resume a previously made virtual session):
>poetry shell

3. Download and install needed package dependencies to this package's virtual environment:
>poetry install


* RUN *

1. Copy the sample configuration to your own
>cp sampleconf.json conf.json

2. Edit `conf.json` with your own username and password - it's assigned in DNS section: https://domains.google.com/registrar/
4. Run the service, which continues indefinitely until you cancel (keyboard: control-c)
>python dyndns.py

===================================================================
