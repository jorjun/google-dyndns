import
  os, sequtils, strformat, strutils, httpclient,
  general

const
  my_IP_url = "https://ipv4bot.whatismyipaddress.com"
  dns_url = "https://domains.google.com"
  interval_minutes = 60
  conf_path = "conf.json"

type
  FailedInit = object of CatchableError
  FailedUpdate = object of CatchableError

  API = object
    client: HttpClient
    config: Config

proc newAPI(): API =
  try:
    API(client: newHttpClient(), config: loadJSON(conf_path))
  except:
    raise newException(FailedInit, fmt"Couldn't parse config: {conf_path}")


method get_ip_address(self: API) : string {.base.} =
  log(fmt"GET: {my_IP_url}")
  self.client.getContent(url=my_IP_url)


method set_ip_address(self: API, ip: string): string {.base.} =
  let
    url = fmt"{dns_url}/nic/update?hostname={self.config.hostname}&myip={ip}"
  log(fmt"POS: {url}")

  self.client.headers = newHttpHeaders({
    "Authorization": get_auth(self.config.username, self.config.password),
    "User-Agent": "@jorjun-ddns"
  })
  self.client.postContent(url)


proc main(self: API): void =
  var
    ip = ""
    old_ip = ""
    status = ""

  while true:
    ip = self.get_ip_address()
    if ip == old_ip:
      log(fmt"IP unchanged: {ip}")
    else:
      status = self.set_ip_address(ip)
      log(fmt"STS: {status}")
      if not @["good", "nochg"].anyIt(status.find(it) > -1):
        raise newException(FailedUpdate, "Error DYNDNS status")
      old_ip = ip

    sleep(interval_minutes * 60 * 1000)


when isMainModule:
  let api = newAPI()
  api.main()
