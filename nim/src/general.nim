import
  times,
  strformat,
  base64,
  json

type
  Config* = object
    hostname*, username*, password*: string

proc log*(msg:string): void =
  let utc = now().utc.format("h:mm:ss tt")
  echo(fmt"GMT {utc} - {msg}")

proc get_auth*(user, pwd: string): string =
  let
    auth = encode(fmt"{user}:{pwd}")
  fmt"Basic {auth}"

proc loadJSON*(path: string): Config =
  let
    data = parseJson(readFile(path))

  Config(
    hostname: data["hostname"].getStr(),
    password: data["password"].getStr(),
    username: data["username"].getStr()
  )
