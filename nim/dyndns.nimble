# Package

version       = "0.1.0"
author        = "Jorjun"
description   = "Updates google hosted dynamic DNS with your external IP"
license       = "MIT"
srcDir        = "src"
binDir        = ".."
bin           = @["dyndns"]


requires "nim >= 1.2.6"
