
Dynamic Domain Name Service
============================

"Updates google-hosted dynamic name service (DNS)"

Uses: ipv4bot.whatismyipaddress.com to obtain your router's current IP address (darn thing keeps changing so you can't access your home Pi when you are elsewhere, right?)
And: https://domains.google.com/nic/update to perform the update.

"Runs every hour, logs to stdout"

* INSTALL *

1. Change to Nim folder:
>cd ~/src/google-dyndns/nim

1. See `nimble` package manager installation: https://github.com/nim-lang/nimble
>nimble build


* RUN *

1. Go to route of checkout. Copy sample configuration
>cp sampleconf.json conf.json

2. Edit `conf.json` with your own username and password - it's assigned for you in DNS section: https://domains.google.com/registrar/
4. Run the service, which continues indefinitely until you cancel (keyboard: control-c)
>./dyndns

===================================================================